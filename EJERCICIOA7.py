EJERICICIO  DE DICCIONARIOS A.7 :
#Dado un número n , seguido de n líneas de texto, imprima todas las palabras encontradas
#en el texto, una por línea, con su número de ocurrencias en el texto. Las palabras deben
#clasificarse en orden descendente según su número de ocurrencias, y todas las palabras
#dentro de la misma frecuencia deben imprimirse en orden lexicográfico.
#Insinuación. Después de crear un diccionario de las palabras y sus frecuencias, le gustaría
#ordenarlo de acuerdo con las frecuencias. Esto se puede lograr si crea una lista cuyos
#elementos son listas de dos elementos: el número de apariciones de una palabra y la palabra
#misma. Por ejemplo, [[2, 'hola'], [1, 'qué'], [3, 'es']] . Luego, la clasificación de lista
#estándar ordenará una lista de listas, con las listas comparadas por el primer elemento, y
#si son iguales, por el segundo elemento. Esto es casi lo que se requiere.
#AUTOR:VICTOR GUAMAN



# Read a string:
# s = input()
# Print a value:
# print(s)
n = int(input())
diccionario = {}
for _ in range(n):
  for palabra in input().split():
    diccionario[palabra] = diccionario.get(palabra, 0) + 1

frecuenciaPa = [(-diccionario, palabra) for (palabra, diccionario) in diccionario.items()]
for c, palabra in sorted(frecuenciaPa):
     print(palabra,c)